package ru.itmo.library.service.config.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import ru.itmo.library.service.model.ReturnNotificationEvent;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Value("${spring.kafka.bootstrap.servers}")
    private String bootstrapServers;

    @Bean
    @Profile("LOCAL")
    public ProducerFactory<String, ReturnNotificationEvent> producerFactoryLocal() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapServers);
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    @Profile("YANDEX")
    public ProducerFactory<String, ReturnNotificationEvent> producerFactoryYandex(@Value("${spring.kafka.username}") String username,
                                                                                  @Value("${spring.kafka.password}") String password,
                                                                                  @Value("${spring.kafka.sasl.mechanism}") String saslMechanism,
                                                                                  @Value("${spring.kafka.login.module}") String loginModule) {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapServers);
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        configProps.putAll(KafkaTopicConfig.securityConfigs(saslMechanism, loginModule, username, password));
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, ReturnNotificationEvent> kafkaTemplate(ProducerFactory<String, ReturnNotificationEvent> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }

}
