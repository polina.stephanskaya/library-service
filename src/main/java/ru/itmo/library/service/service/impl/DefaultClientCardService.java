package ru.itmo.library.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itmo.library.service.exception.ObjectNotFoundException;
import ru.itmo.library.service.mapper.ClientCardMapper;
import ru.itmo.library.service.model.constant.ClientCardStatus;
import ru.itmo.library.service.model.dto.NewClientCard;
import ru.itmo.library.service.model.entity.Book;
import ru.itmo.library.service.model.entity.Client;
import ru.itmo.library.service.model.entity.ClientCard;
import ru.itmo.library.service.repository.BookRepository;
import ru.itmo.library.service.repository.ClientCardRepository;
import ru.itmo.library.service.repository.ClientRepository;
import ru.itmo.library.service.repository.ReturnNotificationEventRepository;
import ru.itmo.library.service.service.api.ClientCardService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DefaultClientCardService implements ClientCardService {

    private final ClientCardRepository clientCardRepository;
    private final BookRepository bookRepository;
    private final ClientRepository clientRepository;

    private final ReturnNotificationEventRepository eventRepository;
    private final ClientCardMapper mapper;

    private final Class<ClientCard> CARD_CLASS = ClientCard.class;

    @Override
    @Transactional
    public ClientCard save(NewClientCard newClientCard) {
        return clientCardRepository
                .findByClient_PhoneNumberAndBook_Isbn(newClientCard.getClientPhone(), newClientCard.getBookIsbn())
                .map(old -> {
                    ClientCard card = clientCardRepository.save(createCard(old.getId(), newClientCard));
                    eventRepository.save(mapper.toNotificationEvent(card));
                    return card;
                })
                .orElse(saveNewCard(newClientCard));
    }

    private ClientCard saveNewCard(NewClientCard newClientCard) {
        ClientCard card = clientCardRepository.save(createCard(null, newClientCard));
        eventRepository.save(mapper.toNotificationEvent(card));
        return card;
    }

    private ClientCard createCard(UUID id, NewClientCard newClientCard) {
        return mapper.fromNewClientCard(newClientCard, id,
                clientRepository.findByPhoneNumber(newClientCard.getClientPhone())
                        .orElseThrow(() -> new ObjectNotFoundException(Client.class)),
                bookRepository.findByIsbn(newClientCard.getBookIsbn())
                        .orElseThrow(() -> new ObjectNotFoundException(Book.class)));
    }

    @Override
    @Transactional
    public ClientCard updateCardStatus(String clientPhone, String bookIsbn, ClientCardStatus status) {
        return clientCardRepository.findByClient_PhoneNumberAndBook_Isbn(clientPhone, bookIsbn)
                .map(old -> {
                    old.setStatus(status);
                    return clientCardRepository.save(old);
                })
                .orElseThrow(() -> new ObjectNotFoundException(CARD_CLASS));
    }

    @Override
    public List<ClientCard> findByClient_PhoneNumber(String clientPhoneNumber) {
        return clientCardRepository.findByClient_PhoneNumber(clientPhoneNumber);
    }

    @Override
    public ClientCard findByClient_PhoneNumberAndBook_Isbn(String clientPhoneNumber, String bookIsbn) {
        return clientCardRepository.findByClient_PhoneNumberAndBook_Isbn(clientPhoneNumber, bookIsbn)
                .orElseThrow(() -> new ObjectNotFoundException(CARD_CLASS));
    }

    @Override
    public List<ClientCard> findByClient_PhoneNumberAndStatus(String clientPhoneNumber, ClientCardStatus status) {
        return clientCardRepository.findByClient_PhoneNumberAndStatus(clientPhoneNumber, status);
    }

    @Override
    public List<ClientCard> findByStatus(ClientCardStatus status) {
        return clientCardRepository.findByStatus(status);
    }

    @Override
    public List<ClientCard> findByReturnDate(LocalDate returnDate) {
        return clientCardRepository.findByReturnDate(returnDate);
    }

}
