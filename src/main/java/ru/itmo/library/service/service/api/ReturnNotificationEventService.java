package ru.itmo.library.service.service.api;

import ru.itmo.library.service.model.constant.EventState;
import ru.itmo.library.service.model.ReturnNotificationEvent;

import java.time.LocalDate;
import java.util.List;

public interface ReturnNotificationEventService {

    ReturnNotificationEvent save(ReturnNotificationEvent event);

    ReturnNotificationEvent delete(ReturnNotificationEvent event);

    List<ReturnNotificationEvent> findByStateAndReturnDate(EventState state, LocalDate returnDate);

}
