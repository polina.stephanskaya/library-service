package ru.itmo.library.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itmo.library.service.model.constant.EventState;
import ru.itmo.library.service.model.ReturnNotificationEvent;
import ru.itmo.library.service.repository.ReturnNotificationEventRepository;
import ru.itmo.library.service.service.api.ReturnNotificationEventService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DefaultReturnNotificationEventService implements ReturnNotificationEventService {

    private final ReturnNotificationEventRepository eventRepository;

    @Override
    public ReturnNotificationEvent save(ReturnNotificationEvent event) {
        return eventRepository.save(event);
    }

    @Override
    @Transactional
    public ReturnNotificationEvent delete(ReturnNotificationEvent event) {
        eventRepository.delete(event);

        return event;
    }

    @Override
    public List<ReturnNotificationEvent> findByStateAndReturnDate(EventState state, LocalDate returnDate) {
        return eventRepository.findByStateAndReturnDate(state, returnDate);
    }

}
