package ru.itmo.library.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itmo.library.service.exception.ObjectNotFoundException;
import ru.itmo.library.service.model.entity.Client;
import ru.itmo.library.service.repository.ClientRepository;
import ru.itmo.library.service.service.api.ClientService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DefaultClientService implements ClientService {

    private final ClientRepository clientRepository;
    private final Class<Client> CLIENT_CLASS = Client.class;

    @Override
    @Transactional
    public Client save(Client client) {
        return clientRepository.findByPhoneNumber(client.getPhoneNumber())
                .map(old -> {
                    client.setId(old.getId());
                    return clientRepository.save(client);
                })
                .orElse(clientRepository.save(client));
    }

    @Override
    public Client findByPhoneNumber(String phoneNumber) {
        return clientRepository.findByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new ObjectNotFoundException(CLIENT_CLASS));
    }

    @Override
    public Client findByPassport(String passport) {
        return clientRepository.findByPassport(passport)
                .orElseThrow(() -> new ObjectNotFoundException(CLIENT_CLASS));
    }

    @Override
    public List<Client> findByBlocked(boolean blocked) {
        return clientRepository.findByBlocked(blocked);
    }

    @Override
    @Transactional
    public Client updateBlocked(String phoneNumber, boolean blocked) {
        return clientRepository.findByPhoneNumber(phoneNumber)
                .map(old -> {
                    old.setBlocked(blocked);
                    return clientRepository.save(old);
                })
                .orElseThrow(() -> new ObjectNotFoundException(CLIENT_CLASS));
    }

}
