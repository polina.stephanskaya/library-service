package ru.itmo.library.service.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itmo.library.service.exception.ObjectNotFoundException;
import ru.itmo.library.service.model.constant.Genre;
import ru.itmo.library.service.model.entity.Book;
import ru.itmo.library.service.model.entity.Client;
import ru.itmo.library.service.repository.BookRepository;
import ru.itmo.library.service.service.api.BookService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DefaultBookService implements BookService {

    private final BookRepository bookRepository;
    private final Class<Client> BOOK_CLASS = Client.class;

    @Override
    @Transactional
    public Book save(Book book) {
        return bookRepository.findByIsbn(book.getIsbn())
                .map(old -> {
                    old.setCount(old.getCount() + 1);
                    old.setAvailable(old.getAvailable() + 1);
                    return bookRepository.save(old);
                })
                .orElse(bookRepository.save(book));
    }

    @Override
    public List<Book> findByNameAndAuthor_FirstNameAndAuthor_LastName(String name,
                                                                      String authorFirstName,
                                                                      String authorLastName) {
        return bookRepository.findByNameAndAuthor_FirstNameAndAuthor_LastName(name, authorFirstName, authorLastName);
    }

    @Override
    public Book findByIsbn(String isbn) {
        return bookRepository.findByIsbn(isbn)
                .orElseThrow(() -> new ObjectNotFoundException(BOOK_CLASS));
    }

    @Override
    public List<Book> findByGenre(Genre genre) {
        return bookRepository.findByGenre(genre);
    }

}
