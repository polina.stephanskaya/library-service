package ru.itmo.library.service.service.api;

import ru.itmo.library.service.model.constant.ClientCardStatus;
import ru.itmo.library.service.model.dto.NewClientCard;
import ru.itmo.library.service.model.entity.ClientCard;

import java.time.LocalDate;
import java.util.List;

public interface ClientCardService {

    ClientCard save(NewClientCard newClientCard);

    ClientCard updateCardStatus(String clientPhone, String bookIsbn, ClientCardStatus status);

    List<ClientCard> findByClient_PhoneNumber(String clientPhoneNumber);

    ClientCard findByClient_PhoneNumberAndBook_Isbn(String clientPhoneNumber, String bookIsbn);

    List<ClientCard> findByClient_PhoneNumberAndStatus(String clientPhoneNumber, ClientCardStatus status);

    List<ClientCard> findByStatus(ClientCardStatus status);

    List<ClientCard> findByReturnDate(LocalDate returnDate);

}
