package ru.itmo.library.service.service.api;

import ru.itmo.library.service.model.entity.Client;

import java.util.List;

public interface ClientService {

    Client save(Client client);

    Client findByPhoneNumber(String phoneNumber);

    Client findByPassport(String passport);

    List<Client> findByBlocked(boolean blocked);

    Client updateBlocked(String phoneNumber, boolean blocked);

}
