package ru.itmo.library.service.service.api;

import ru.itmo.library.service.model.constant.Genre;
import ru.itmo.library.service.model.entity.Book;

import java.util.List;

public interface BookService {

    Book save(Book book);

    List<Book> findByNameAndAuthor_FirstNameAndAuthor_LastName(String name,
                                                               String authorFirstName,
                                                               String authorLastName);

    Book findByIsbn(String isbn);

    List<Book> findByGenre(Genre genre);

}
