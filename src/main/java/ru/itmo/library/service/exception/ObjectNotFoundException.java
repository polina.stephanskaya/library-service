package ru.itmo.library.service.exception;

public class ObjectNotFoundException extends RuntimeException {

    public <T> ObjectNotFoundException(Class<T> className) {
        super(className.getSimpleName() + " was not found");
    }

}
