package ru.itmo.library.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.library.service.model.constant.Genre;
import ru.itmo.library.service.model.entity.Book;
import ru.itmo.library.service.service.api.BookService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService service;

    @PostMapping
    Book saveNewBook(@RequestBody @Valid Book book) {
        return service.save(book);
    }

    @GetMapping
    List<Book> findBooksByNameAndAuthorFirstNameAndAuthorLastName(@RequestParam String name,
                                                                 @RequestParam String authorFirstName,
                                                                 @RequestParam String authorLastName) {
        return service.findByNameAndAuthor_FirstNameAndAuthor_LastName(name, authorFirstName, authorLastName);
    }

    @GetMapping("/isbn")
    Book findBookByIsbn(@RequestParam String isbn) {
        return service.findByIsbn(isbn);
    }

    @GetMapping("/genres")
    List<Book> findBooksByGenre(@RequestBody Genre genre) {
        return service.findByGenre(genre);
    }

}
