package ru.itmo.library.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.library.service.model.entity.Client;
import ru.itmo.library.service.service.api.ClientService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService service;

    @PostMapping
    Client saveNewClient(@RequestBody @Valid Client client) {
        return service.save(client);
    }

    @GetMapping("/phone")
    Client findClientByPhoneNumber(@RequestParam String phoneNumber) {
        return service.findByPhoneNumber(phoneNumber);
    }

    @GetMapping("/passport")
    Client findClientByPassport(@RequestParam String passport) {
        return service.findByPassport(passport);
    }

    @GetMapping("/blocked")
    List<Client> findClientsByBlocked(@RequestParam boolean blocked) {
        return service.findByBlocked(blocked);
    }

    @PutMapping("/blocked")
    Client changeClientBlockedStatus(@RequestParam String phoneNumber, @RequestParam boolean blocked) {
        return service.updateBlocked(phoneNumber, blocked);
    }

}
