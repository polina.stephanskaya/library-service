package ru.itmo.library.service.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.library.service.model.constant.ClientCardStatus;
import ru.itmo.library.service.model.dto.NewClientCard;
import ru.itmo.library.service.model.entity.ClientCard;
import ru.itmo.library.service.service.api.ClientCardService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/cards")
@RequiredArgsConstructor
public class ClientCardController {

    private final ClientCardService service;

    @PostMapping
    ClientCard saveNewCards(@RequestBody @Valid NewClientCard newClientCard) {
        return service.save(newClientCard);
    }

    @PutMapping
    ClientCard updateCardStatus(@RequestParam String clientPhone,
                                @RequestParam String bookIsbn,
                                @RequestParam ClientCardStatus status) {
        return service.updateCardStatus(clientPhone, bookIsbn, status);
    }

    @GetMapping("/clients")
    List<ClientCard> findCardsByClientPhoneNumber(@RequestParam String clientPhoneNumber) {
        return service.findByClient_PhoneNumber(clientPhoneNumber);
    }

    @GetMapping
    ClientCard findCardByClientPhoneNumberAndBookIsbn(@RequestParam String clientPhoneNumber,
                                                      @RequestParam String bookIsbn) {
        return service.findByClient_PhoneNumberAndBook_Isbn(clientPhoneNumber, bookIsbn);
    }

    @GetMapping("/clients/status")
    List<ClientCard> findCardsByClientPhoneNumberAndStatus(@RequestParam String clientPhoneNumber,
                                                           @RequestParam ClientCardStatus status) {
        return service.findByClient_PhoneNumberAndStatus(clientPhoneNumber, status);
    }

    @GetMapping("/status")
    List<ClientCard> findCardsByStatus(@RequestParam ClientCardStatus status) {
        return service.findByStatus(status);
    }

    @GetMapping("/date")
    List<ClientCard> findByReturnDate(@RequestParam LocalDate returnDate) {
        return service.findByReturnDate(returnDate);
    }

}
