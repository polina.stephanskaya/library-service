package ru.itmo.library.service.sender;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import ru.itmo.library.service.model.constant.EventState;
import ru.itmo.library.service.model.ReturnNotificationEvent;

@Component
@RequiredArgsConstructor
public class NotificationEventMessageSender {

    private final KafkaTemplate<String, ReturnNotificationEvent> kafkaTemplate;

    @Value("${spring.kafka.template.default-topic}")
    private String topic;

    @Value("${event-sender.max-attempts}")
    private int maxAttempts;

    @SneakyThrows
    public ReturnNotificationEvent send(ReturnNotificationEvent event) {

       return kafkaTemplate.send(topic, event.getPhoneNumber(), event)
               .completable()
               .handle((msg, ex) -> {
                   if (ex != null) {
                       System.out.println("Unable to send message=["
                               + msg + "] due to : " + ex.getMessage());
                       event.setAttempts(event.getAttempts() + 1);
                       if (event.getAttempts() >= maxAttempts) {
                           event.setState(EventState.FAILED);
                       }
                   } else {
                       System.out.println("Sent message=[" + msg +
                               "] with offset=[" + msg.getRecordMetadata().offset() + "]");
                       event.setAttempts(event.getAttempts() + 1);
                       event.setState(EventState.COMPLETED);
                   }
                   return event;
               })
               .get();
    }

}
