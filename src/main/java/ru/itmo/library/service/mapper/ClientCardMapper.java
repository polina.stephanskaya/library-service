package ru.itmo.library.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.library.service.model.dto.NewClientCard;
import ru.itmo.library.service.model.entity.Book;
import ru.itmo.library.service.model.entity.Client;
import ru.itmo.library.service.model.entity.ClientCard;
import ru.itmo.library.service.model.ReturnNotificationEvent;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface ClientCardMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "status", expression = "java(ru.itmo.library.service.model.constant.ClientCardStatus.OPEN)")
    ClientCard fromNewClientCard(NewClientCard newClientCard, UUID id, Client client, Book book);

    @Mapping(target = "id", expression = "java(null)")
    @Mapping(target = "phoneNumber", expression = "java(clientCard.getClient().getPhoneNumber())")
    @Mapping(target = "book", expression = "java(clientCard.getBook().getFullName())")
    @Mapping(target = "clientName", expression = "java(clientCard.getClient().getFullName())")
    @Mapping(target = "attempts", expression = "java(0)")
    @Mapping(target = "state", expression = "java(ru.itmo.library.service.model.constant.EventState.CREATED)")
    @Mapping(target = "lastErrorMessage", expression = "java(null)")
    ReturnNotificationEvent toNotificationEvent(ClientCard clientCard);

}
