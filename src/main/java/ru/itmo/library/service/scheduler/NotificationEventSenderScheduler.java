package ru.itmo.library.service.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.itmo.library.service.model.constant.EventState;
import ru.itmo.library.service.sender.NotificationEventMessageSender;
import ru.itmo.library.service.service.api.ReturnNotificationEventService;

import java.time.LocalDate;

@EnableAsync
@Service
@RequiredArgsConstructor
public class NotificationEventSenderScheduler {

    private final NotificationEventMessageSender messageSender;
    private final ReturnNotificationEventService eventService;

    @Async
    @Scheduled(cron = "${event-sender.cron.expression}")
    public void processSender() {
        eventService.findByStateAndReturnDate(EventState.CREATED, LocalDate.now().plusDays(1))
                .forEach(event -> {
                    event = messageSender.send(event);
                    eventService.save(event);
                });
    }

}
