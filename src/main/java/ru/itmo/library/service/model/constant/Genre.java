package ru.itmo.library.service.model.constant;

public enum Genre {

    FANTASY("Фэнтези"),
    SCIENCE_FICTION("Научная фантастика"),
    MYSTERY("Мистика"),
    ROMANCE("Роман"),
    CHILDREN("Для детей"),
    NONFICTION("Документальная литература");

    private final String name;
    Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}