package ru.itmo.library.service.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import ru.itmo.library.service.model.constant.EventState;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "return_notification_event")
public class ReturnNotificationEvent {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "book")
    private String book;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "return_date")
    private LocalDate returnDate;

    @Column(name = "attempts")
    private int attempts;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EventState state;

    @Column(name = "last_error_message")
    private String lastErrorMessage;

    @LastModifiedDate
    @Column(name = "modified_date")
    private LocalDate modifiedDate;

}
