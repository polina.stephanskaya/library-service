package ru.itmo.library.service.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import ru.itmo.library.service.model.constant.Genre;

import javax.persistence.*;
import java.util.UUID;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "author_id")
    @Cascade(CascadeType.ALL)
    private Author author;

    @Column(name = "genre")
    @Enumerated(EnumType.STRING)
    private Genre genre;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "count")
    private int count;

    @Column(name = "available")
    private int available;

    public String getFullName() {
        return author.getFullName() + " " + "\"" + name + "\"" + " ISBN: " + isbn;
    }

}
