package ru.itmo.library.service.model.constant;

public enum EventState {
    CREATED,
    COMPLETED,
    FAILED
}
