package ru.itmo.library.service.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewClientCard {

    private String clientPhone;
    private String bookIsbn;
    private LocalDate takingDate;
    private LocalDate returnDate;

}
