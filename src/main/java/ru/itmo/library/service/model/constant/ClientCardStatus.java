package ru.itmo.library.service.model.constant;

public enum ClientCardStatus {
    OPEN,
    CLOSED,
    OVERDUE,
    UNKNOWN
}
