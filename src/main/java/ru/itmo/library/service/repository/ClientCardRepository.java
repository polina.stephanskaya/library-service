package ru.itmo.library.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.library.service.model.constant.ClientCardStatus;
import ru.itmo.library.service.model.entity.ClientCard;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientCardRepository extends JpaRepository<ClientCard, UUID> {

    List<ClientCard> findByClient_PhoneNumber(String clientPhoneNumber);

    Optional<ClientCard> findByClient_PhoneNumberAndBook_Isbn(String clientPhoneNumber, String bookIsbn);

    List<ClientCard> findByClient_PhoneNumberAndStatus(String clientPhoneNumber, ClientCardStatus status);

    List<ClientCard> findByStatus(ClientCardStatus status);

    List<ClientCard> findByReturnDate(LocalDate returnDate);

}
