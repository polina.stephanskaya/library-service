package ru.itmo.library.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.library.service.model.constant.Genre;
import ru.itmo.library.service.model.entity.Book;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {

    List<Book> findByNameAndAuthor_FirstNameAndAuthor_LastName(String name,
                                                               String authorFirstName,
                                                               String authorLastName);

    Optional<Book> findByIsbn(String isbn);

    List<Book> findByGenre(Genre genre);

}
