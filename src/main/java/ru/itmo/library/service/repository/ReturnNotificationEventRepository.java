package ru.itmo.library.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.library.service.model.constant.EventState;
import ru.itmo.library.service.model.ReturnNotificationEvent;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface ReturnNotificationEventRepository extends JpaRepository<ReturnNotificationEvent, UUID> {

    List<ReturnNotificationEvent> findByStateAndReturnDate(EventState state, LocalDate returnDate);

}
