package ru.itmo.library.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.library.service.model.entity.Client;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<Client, UUID> {

    Optional<Client> findByPhoneNumber(String phoneNumber);

    Optional<Client> findByPassport(String passport);

    List<Client> findByBlocked(boolean blocked);

}
