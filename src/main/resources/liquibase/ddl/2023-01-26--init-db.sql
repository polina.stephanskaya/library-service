CREATE TABLE client
(
    id           UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    first_name   VARCHAR NOT NULL,
    second_name  VARCHAR,
    last_name    VARCHAR NOT NULL,
    birthday     DATE    NOT NULL,
    passport     VARCHAR NOT NULL UNIQUE,
    phone_number VARCHAR NOT NULL UNIQUE,
    blocked      BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE author
(
    id          UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    first_name  VARCHAR NOT NULL,
    second_name VARCHAR,
    last_name   VARCHAR NOT NULL
);

CREATE TABLE book
(
    id        UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name      VARCHAR NOT NULL,
    author_id UUID    NOT NULL REFERENCES author (id) ON DELETE CASCADE,
    genre     VARCHAR NOT NULL DEFAULT 'UNKNOWN',
    isbn      VARCHAR NOT NULL UNIQUE,
    count     INT     NOT NULL DEFAULT 1,
    available INT     NOT NULL
);

CREATE TABLE client_card
(
    id          UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    client_id   UUID    NOT NULL REFERENCES client (id),
    book_id     UUID REFERENCES book (id) ON DELETE CASCADE,
    taking_date DATE    NOT NULL,
    return_date DATE    NOT NULL,
    status      VARCHAR NOT NULL DEFAULT 'OPEN'
);