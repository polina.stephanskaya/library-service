CREATE TABLE return_notification_event
(
    id                 UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    phone_number       VARCHAR NOT NULL,
    book               VARCHAR NOT NULL,
    client_name        VARCHAR NOT NULL,
    return_date        DATE    NOT NULL,
    attempts           INT     NOT NULL DEFAULT 0,
    state              VARCHAR NOT NULL DEFAULT 'CREATED',
    last_error_message VARCHAR,
    modified_date      DATE
);